const jwt = require('jsonwebtoken');
const secret = 'CourseBookingAPI';

module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {})
}

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;

	console.log(token);

	if (typeof token !== 'undefined') {

		// 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmYjUwOTdmMDc3MjliMGRlMDBhOTdkMSIsImVtYWlsIjoiYXJ2aW5AZ21haWwuY29tIiwiaXNBZG1pbiI6ZmFsc2UsImlhdCI6MTYwNTg3NzE4M30.ds61cxFzR6Gk3Sh3D3PkvgAjMr7GBUu_piIk9uRZGcg'
		token = token.slice(7, token.length);

		console.log(token);

		return jwt.verify(token, secret, (err, data) => {
			// next() passes the request to next callback function/middleware
			return (err) ? res.send({ auth: 'failed' }) : next()
		})

		// sayBye = callback function
		// function sayHello (param1){
		// }
		// sayHello(param1, sayBye())

	} else {
		return res.send({ auth: 'failed' })
	}
}

// asdfweiu1234 = hello123
// 128j91j2e8j1 = hello123

module.exports.decode = (token) => {
	if (typeof token !== 'undefined'){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			// {complete: true} grabs both the request header and the payload
			return (err) ? null : jwt.decode(token, {complete: true}).payload
		})
	} else {
		return null
	}
}