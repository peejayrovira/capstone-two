const User = require('../models/User')
const Course = require('../models/Course')
const bcrypt = require("bcrypt")
const auth = require('../auth')

//Check if email exists
module.exports.emailExists = (params) => {
	return User.find({email: params.email}).then(resultFromFind => {
		return resultFromFind.length > 0 ? true : false
	})
}

//User Registration
module.exports.register = (params) => {
	let newUser = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10) //hashSync() hashes/encrypts and the number is the salt value or how many times the password is hashed
	})

	return newUser.save().then((user, err) => {
		return (err) ? false : true
		//if there is an error in registration, return false. Otherwise, return true
	})
}

//login
module.exports.login = (params) => {
	return User.findOne({email: params.email}).then(resultFromFindOne => {
		if(resultFromFindOne === null){ //user doesn't exist
			return false
		}

		const isPasswordMatched = bcrypt.compareSync(params.password, resultFromFindOne.password) //decrypts password

		// params = arvin123
		// resultFromFindOne = $2a$10$n8WGApDG.zHVr7OrP2s.S.WIoB8LQvATxD.wbbxh/gmIJia40ABDm

		// console.log(resultFromFindOne.save);
		// console.log(resultFromFindOne.toObject().save);

		if(isPasswordMatched){
			return {
				accessToken: auth.createAccessToken(resultFromFindOne.toObject())
			}
		} else {
			return false
		}
	})
}

module.exports.get = (params) => {
	return User.findById(params.userId).then(user => {
		// re-assign the password to undefined so it won't be displayed along with other user data
		user.password = undefined
		return user
	})
}

module.exports.enroll = (params) => {
	// find a user
	return User.findById(params.userId).then(user => {
		// add the course ID to the user
		user.enrollments.push({ courseId: params.courseId})

		// save to the database
		return user.save().then((user, err) => {
			// find a course
			return Course.findById(params.courseId).then(course => {
				// add the user ID to the course
				course.enrollees.push({ userId: params.userId })

				// save to database
				return course.save().then((course, err) => {
					return (err) ? false : true
				})
			})
		})
	})
}