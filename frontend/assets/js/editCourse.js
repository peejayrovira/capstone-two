let editCourse = document.querySelector('#editCourse');

editCourse.addEventListener("submit", (e) => {

	e.preventDefault()

	let courseName = document.querySelector("#courseName").value;
	let courseDescription = document.querySelector("#courseDescription").value;
	let coursePrice = document.querySelector("#coursePrice").value;

	let token = localStorage.getItem("token");


	fetch(`http://localhost:4000/api/courses/${course._id}`, {

		method: 'PUT',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			name: courseName,
			description: courseDescription,
			price: coursePrice
		})
	})

	console.log(courseName);
	console.log(coursePrice);
	console.log(courseDescription);

	let formEditCourse;

		formEditCourse = 

		`

		<form id="editCourse">	
			<div class="form-row">
				<div class="form-group col-md-9">
					<input type="text" id="courseName" class="form-control" placeholder="${courseName}">
				</div>

				<div class="form-group col-md-3">
					<input type="number" id="coursePrice" class="form-control" placeholder="${coursePrice}">
				</div>
			</div>

			<input type="text" id="courseDescription" class="form-control" placeholder="${courseDescription}">
			<!-- end form row -->

			<button type="submit" class="btn btn-block btn-primary my-3"> Submit </button>
		</form>

		`


	.then(res, res.json())
	.then(data => {

		console.log(data);

		
	})